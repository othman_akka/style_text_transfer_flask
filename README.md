# model of Style text transfer with Flask
This project of style text transfer with deep learning.
<br>this repo contains a data in /data/yelp folder.
<br>Style Transfer from Non-Parallel Text by Cross-Alignment". Tianxiao Shen, Tao Lei, Regina Barzilay, and Tommi Jaakkola. NIPS 2017.[data](https://arxiv.org/abs/1705.09655).
<br>Given positive and negative reviews as two corpora, the model can learn to reverse the sentiment of a sentence.
<br>for more informations or see examples visit /img folder

## Dependencies
tensorflow CPU 1.13.1 or tensorflow-gpu 1.13.1

## Quick Start
* To train a model ,first go to the code/ folder and run the following command:
```python
python style_transfer.py --train ../data/yelp/sentiment.train --dev ../data/yelp/sentiment.dev --output ../tmp/sentiment.dev --vocab ../tmp/yelp.vocab --model ../tmp/model
```
In Google Colab :
```python
!python style_transfer.py --train ../data/yelp/sentiment.train --dev ../data/yelp/sentiment.dev --output ../tmp/sentiment.dev --vocab ../tmp/yelp.vocab --model ../tmp/model
```
* To test the model, run the following command:
```python
python style_transfer.py --test ../data/yelp/sentiment.test --output ../tmp/sentiment.test --vocab ../tmp/yelp.vocab --model ../tmp/model --load_model true --beam 8
```
In Google Colab
```python
!python style_transfer.py --test ../data/yelp/sentiment.test --output ../tmp/sentiment.test --vocab ../tmp/yelp.vocab --model ../tmp/model --load_model true --beam 8
```
## Excution
* In Google Colab :
make this project in drive and go to /code folder and open edit.ipynb, then excute each command.
* In IDE (pycharm or Anaconda ...) :
<br>Install the requirement by
```bash
    pip install -r requirements.txt
```
if you updated the requirement please update the requirements.txt by:
```bash
    pip freeze > requirements.txt
```

