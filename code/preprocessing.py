import glob
import ntpath

def preprocessing():
    for file in glob.glob("../data2/data_1/*.txt"):
        f = open(file, "r")
        with open("../data2/data_1_proce/"+ntpath.basename(file), "w+") as new_file:
            for line in f:
                line = [line.split()[i] for i in range(2,len(line.split()))]
                line = ' '.join([str(word) for word in line])
                print(line)
                new_file.write(str(line)+"\n")

if __name__=='__main__':
    preprocessing()