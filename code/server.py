import json
from flask import Flask, render_template, request
from werkzeug.exceptions import abort

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/web', methods=['POST'])
def worker():
    data = request.get_json()
    # response of our model
    response = "response generated"
    return json.dumps(response)

@app.route('/mobile', methods=['POST'])
def worker2():
    data = request.get_json()
    print(type(data))
    if str(data["text"]) == "connect":
        return json.dumps(data)
    else:
        response = "response generated"
        return json.dumps({"response": response})

if __name__ == '__main__':
    #app.run()
    app.run(host="192.168.1.86", port=5000, debug=True, use_reloader=False)