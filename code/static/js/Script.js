function insert_chat(text) {
    $("#new").html("").scrollTop($("").prop('scrollHeight'));
    $("#new").append(text).scrollTop($("textarea").prop('scrollHeight'));
}

$("#send").on("click", function () {
    var t = $("#original")[0];
    var txt = $(t).val().trim();
    if (txt != "") {
        //$(t).val('');
        send_req(txt);
    }
});

function send_req(txt) {
    $.ajax({
        type: "POST",
        url: "/web",
        data: JSON.stringify(txt),
        dataType: 'json',
        contentType: 'application/json;charset=UTF-16',
        success: function (res) {
            recieve_res(res);
        }
    });
}
function recieve_res(res) {
    var msg = res;
    insert_chat(msg);
}
